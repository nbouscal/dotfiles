source ~/.antigen/antigen.zsh
antigen-lib
antigen-bundle zsh-users/zsh-syntax-highlighting
antigen-theme lambda
antigen-apply

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
